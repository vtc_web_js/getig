$(window).load(function () {
    var ww = $(window).width();
    var h = $('div.container-fluid').height();
    $('.cover').css({'background': "white"}).css({'width': ww}).css({'height': h});
    load();
    var pic = $('.pic img').width(), nav =$('nav').width(),container = $('.container-fluid').width(), w = $(window).width();
    $('.main').css({'width': (container - 36)});
    var pic_w = $('.pic img').width();
    var main_w = $('.main').width();
    if(w<1199){
        $('.content>div:first-child').css({'min-width': ""}).css({'width': "100%"});
        $('.header').css({'width': w*65/100});
        $('.sub_header').css({'width': w*65/100});
    }else{
        $('.content>div:first-child').css({'width': main_w - pic_w}).css({'min-width': ""});
        $('.header').css({'width': w - 85- 412});
        $('.sub_header').css({'width': w - 85- 412});
    }
   }).resize(function () {
    var container = $('.container-fluid').width();
    $('.main').css({'width': (container - 36)});
    var w =$(window).width();
    load();
    sizes();
    $('.header').css({'width': w*65/100});
    $('.sub_header').css({'width': w*65/100});
   });
function load() {
    var body = $('body').height();
    var h = $(window).height();
    var w = $(window).width();
    var nav = $('nav');
    var content= $('div.content').height();
    var pic = $('.pic img').width();
    var main =$('.main').height();
    var container = $('.container-fluid').width();
    if(w>1199){
        if(h>580){
            $(nav).css({'height': h}).show();
            $('.carousel-inner .item img').css({'height': h*81/100});
            $('#main').css({'height': h - 300});
            $('.carousel-inner').css({'height': h-230}).css({'max-height': h-230});
        }else{
            if(h>main){
                $(nav).css({'height': h}).show();
            }else{
                $(nav).css({'height': main}).show();
            }
            $('.carousel-inner .item img').css({'height': "510px"});
            $('#main').css({'height': "300px"});
            $('.carousel-inner').css({'height': "300px"});
        }
    }else{
        if(h>main){
            $(nav).css({'height': h}).show();
        }else{
            $(nav).css({'height': main}).show();
        }
        $('.carousel-inner .item img').css({'height': "300px"});
        $('#main').css({'height': "250px"});
        $('.carousel-inner').css({'height': "250px"});
        $('.content>div:first-child').css({'min-width': ""}).css({'width': "100%"});
        $('.header').css({'width': w});
        $('.sub_header').css({'width': w});
    }
    $('.cover').remove();
}
function sizes(){
    var w = $(window).width();
    var main =$('.main').width();
    var pic = $('.pic').width();
    if(w>1199){
        $('.content>div:first-child').css({'width': main - pic});
        $('.header').css({'width': w - 59- pic});
        $('.sub_header').css({'width': w - 59- pic});
    }
}
$('.mobile_menu').on('click', function () {
    var nav = $('nav');
    var  nav_w = $(nav).width();
    var  w = $(window).width();
    if(nav_w<100){
        if(w>767){
            $(nav).removeClass('animate_left').css({'animation-name': "animate_right"}).addClass('animation_menu');
            setTimeout(function () {
                $('#menu').show();
                $(nav).css({'width': "500px"});
            },400);
        }else{
            $(nav).removeClass('animate_left').css({'animation-name': "animate_mobile_right"}).addClass('animation_mobile_menu');
            setTimeout(function () {
                $('#menu').show();
                $(nav).css({'width': "300px"});
            },400);
        }
        $('.navbar-brand').hide();
        $('.mobile_menu').removeClass('text-center').addClass('text-left').css({'border-bottom': "1px solid transparent"});
        $('.share>div').css({'float': "left"}).css({'margin-left': "15px"});
        $('.icon_bars>img').hide();
        $('.mobile_menu>div').append('<img class="close_icon" src="icons/icon_close.png">');
        $('.share').hide();
    }else{
        $('.mobile_menu').removeClass('text-left').addClass('text-center').css({'border-bottom': "1px solid #3a3a3a"});
        $('.share>div').css({'float': ""}).css({'margin-left': ""});
        $(nav).removeClass('animation_menu').css({'animation-name': "animate_left"}).addClass('animate_left');
        $('#menu').hide();
        $('.icon_bars>img').show();
        $('.share').show();
        $('.navbar-brand').show();
        var svg_close = $('img.close_icon');
        $(svg_close).remove();

        setTimeout(function () {
            $(nav).css({'width': "85px"});
            $(nav).css({'width': ""});
        },400)
    }
});

// Scroll event
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    var nav = $('nav');
    var h = $(window).height();
    var height = $('.container-fluid').height();
    if((scroll<height - h)){
        $(nav).css({'height': height + scroll});
    }
});